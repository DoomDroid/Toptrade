export function routerConfig($stateProvider, $urlRouterProvider) {
  "ngInject";
  $stateProvider
    .state("emitterlist", {
      url: "/",
      component: "SharesEmittersComponent",
      template: "<shares-emitter></shares-emitter>",
      resolve: {
        home: function (SharesEmittersService) {
          return SharesEmittersService.getEmittersList()
        }
      }
    })
    .state("emitterlist.emitter", {
      url: "{shEmitId}",
      component: "EmitterComponent",
      template: "<emitter-comp></emitter-comp>",
      resolve: {
        emitter: function (home, $stateParams) {
          return home.find(function (shEmitter) {
            return shEmitter.id === $stateParams.shEmitId;
          })
        }
      }
    });
  $urlRouterProvider.otherwise("/");
}
