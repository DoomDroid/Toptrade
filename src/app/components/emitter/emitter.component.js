const SERVICE = new WeakMap();

export const EmitterComponent = {

  templateUrl: "app/components/emitter/emitter.html",
  styleUrls: ["app/components/emitter/emitter.css"],
  bindings: {
    idEmitter: "<",
    logoEmitter: "<",
    nameEmitter: "<",
    descriptionEmitter: "<",
    priceEmitter: "<",
  },
  controllerAs: "EmitCtr",
  controller: class emitterController {
    constructor($stateParams, SharesEmittersService, $interval) {
      "ngInject";
      this.$stateParams = $stateParams;
      this.$interval = $interval;
      // this.shEmitterInfo;
      this.graphData;
      this.toggle = false;
      SERVICE.set(this, SharesEmittersService); //private

    }

    $onInit() {
      // this.getemitor();
      // this.getGraphData();
    }
    showGraph(shEmitId) {
      this.toggle = !this.toggle;
      this.reloadGraphData(shEmitId);
    }
    // getemitor() {
    //   this.SharesEmittersService.serchEmitter(this.$stateParams.shEmitId)
    //     .then(result => this.shEmitterInfo = result)
    // }

    // getGraphData() {
    //   this.SharesEmittersService.getGraphData(this.$stateParams.shEmitId)
    //     .then(result => {
    //       this.graphData = result;
    //     })
    // }
    reloadGraphData(shEmitId) {
      this.$interval((shEmitId) => this.getGraphData(shEmitId), 30000);
    }
    getGraphData(shEmitId) {
      SERVICE.get(this).getGraphData(shEmitId)
        .then(result => {
          this.graphData = result;
        })
    }

  }
};


