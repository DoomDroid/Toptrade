export function graphDirective(d3) {
  "ngInject";

  let directive = {
    restrict: "E",
    template: `<div class='graph'></div>`,
    scope: {
      initialData: "@"
    },
    link: function (scope, element, attrs) {
      let dataDB = [];
      let timeRange = 5 * 60 * 1000; // 5 min
      let timeshift = 30 * 1000;    // 30 sec
      let margin = {top: 0, right: 20, bottom: 8, left: 50};
      let width = 175, height = 75;
      let graphWidth = width - margin.right - margin.left;
      let graphHeight = height - margin.top - margin.bottom;
      let color = {first: "red", second: "yellow", third: "white"};

      let x = d3.time.scale()
        .domain([Date.now() - timeRange, Date.now() - timeshift])
        .range([0, graphWidth]);
      let xAxis = d3.svg.axis().scale(x).orient("bottom");
      let y = d3.scale.linear()
        .domain([200, 2000])
        .range([graphHeight, 0]);
      let yAxis = d3.svg.axis().scale(y).orient("left").ticks(3);

      attrs.$observe("initialData", function (value) {
        let initialData = JSON.parse(value);
        initialData.map(obj => dataDB.push(obj));
        let minY = d3.min(dataDB, function (d) {
          return d.price;
        });
        let maxY = d3.max(dataDB, function (d) {
          return d.price;
        });
        let differ = maxY - minY;
        let minDeffer = 100;

        if ((maxY - minY) < minDeffer) {
          minY -= differ / 2;
          maxY += differ / 2;
          if (minY < 200) {
            minY = 200;
            maxY = 200 + minDeffer;
          } else if (maxY > -50) {
            minY = -50;
            maxY = -50 + minDeffer;
          }
        }

        y = d3.scale.linear()
          .domain([minY, maxY])
          .range([graphHeight, 0]);

        yAxis = d3.svg.axis().scale(y).orient("left").ticks(3);
        tick();
      });

      let area = d3.svg.area()
        .x(function (d) {
          return x(d.timeMark);
        })
        .y0(graphHeight)
        .y1(function (d) {
          return y(d.price);
        });

      let svg = d3.select(element[0]).select(".graph")
        .append("svg")
        .attr("class", "chart")
        .attr("width", width)
        .attr("height", height);

      svg.append("defs").append("clipPath")
        .attr("id", "clip")
        .append("rect")
        .attr("width", graphWidth)
        .attr("height", graphHeight)
        .attr("x", margin.left)
        .attr("y", margin.top);

      svg.append("linearGradient")
        .attr("id", "blue-gradient")
        .attr("gradientUnits", "userSpaceOnUse")
        .attr("x1", 0).attr("y1", -55)
        .attr("x2", 0).attr("y2", 100)
        .selectAll("stop")
        .data([
          {offset: "0%", color: color.first},
          {offset: "50%", color: color.second},
          {offset: "100%", color: color.third}
        ])
        .enter().append("stop")
        .attr("offset", function (d) {
          return d.offset;
        })
        .attr("stop-color", function (d) {
          return d.color;
        });

      let renderXAxis = svg.append("g")
        .attr("class", "x axis")
        .attr("transform", "translate(" + margin.left + "," + graphHeight + ")")
        .call(xAxis);

      let renderYAxis = svg.append("g")
        .attr("class", "y axis")
        .attr("transform", "translate(" + margin.left + ")")
        .call(yAxis);

      let path = svg.append("g")
        .attr("clip-path", "url(#clip)")
        .append("path")
        .datum(dataDB)
        .attr("class", "area")
        .attr("d", area);

      function tick() {
        x.domain([Date.now() - timeRange, Date.now() - timeshift]);

        path
          .attr("d", area)
          .attr("transform", null)
          .transition()
          .duration(timeshift)
          .ease("linear")
          .attr("transform", "translate(" + x(Date.now() - timeRange - timeshift) + ",0)")
          .each("end", tick);

        renderXAxis
          .transition()
          .duration(timeshift)
          .ease("linear")
          .call(xAxis);

        renderYAxis
          .transition()
          .duration(timeshift)
          .ease("linear")
          .call(yAxis);


        // pop the old data point off the front
        if (dataDB.length > 1000) {
          dataDB.shift();
        }


      }
    }
  };

  return directive;
}


