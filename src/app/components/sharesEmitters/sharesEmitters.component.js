const SERVICE = new WeakMap();

export const SharesEmittersComponent = {
  templateUrl: "app/components/sharesEmitters/sharesEmitters.html",
  bindings: { shAttr1: "<" },
  controllerAs: "shCtr",
  controller: class sharesEmittersController {
    constructor(SharesEmittersService, $interval, $location) {
      "ngInject";
      this.$location = $location;
      this.$interval = $interval;
      this.ShData = [];
      this.ShEmmitter;
      SERVICE.set(this, SharesEmittersService); //private
    }

    $onInit() {
      this.fetchData();
      this.reloadData();
    }
    searchEmitter(shEmitId) {
      SERVICE.get(this).searchEmitter(shEmitId).then(obj => this.ShEmmitter = obj)
        .catch(function (error) {
          console.log("Component failed searchEmitter.\n" + angular.toJson(error.data, true));
        })
    }

    reloadData() {
      this.$interval(() => {
        this.fetchData()
      }, 10000);           // 10sec
    }

    fetchData() {
      SERVICE.get(this).getEmittersList()
        .then(obj => {
          obj.map(x => {
            x.price = Math.round(1000 * Math.random() + 1010);
            x.timestamp = Date.now();
            return obj
          });
          this.ShData = obj;
          this.$apply;
        }, error => {
          console.log("error wen contoler fetch data.\n" + angular.toJson(error.data, true));
        })
    }
  }

};


