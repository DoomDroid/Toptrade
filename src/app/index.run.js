export function runBlock($log, $httpBackend, $http) {
  "ngInject";
  let data2 = [
    {
      "id": 1,
      "name": "AngularJS",
      "description": "HTML enhanced for web apps!",
      "logo": "angular.png",
    },
    {
      "id": 2,
      "name": "BrowserSync",
      "description": "Synchronised browser testing.",
      "logo": "browsersync.png",
    },
    {
      "id": 3,
      "name": "GulpJS",
      "description": "The streaming build system.",
      "logo": "gulp.png",
    },
    {
      "id": 4,
      "name": "Jasmine",
      "description": "Behavior-Driven JavaScript.",
      "logo": "jasmine.png",
    },
    {
      "id": 5,
      "name": "Karma",
      "description": "Spectacular Test Runner for JavaScript.",
      "logo": "karma.png",
    },
    {
      "id": 6,
      "name": "Protractor",
      "description": "End to end test framework for AngularJS applications built on top of WebDriverJS.",
      "logo": "protractor.png",
    },
    {
      "id": 7,
      "name": "Angular Material Design",
      "description": "The Angular reference implementation of the Google\"s Material Design specification.",
      "logo": "angular-material.png",
    },
    {
      "id": 8,
      "name": "Sass (Node)",
      "description": "Node.js binding to libsass, the C version of the popular stylesheet preprocessor, Sass.",
      "logo": "node-sass.png",
    },
    {
      "id": 9,
      "name": "ES6 (Babel formerly 6to5)",
      "description": "Turns ES6+ code into vanilla ES5, so you can use next generation features today.",
      "logo": "babel.png",
    }
  ];
  // let rundomData = chance.natural({min: 200, max: 2000});

  function generatorSharesList(data) {
    let mynewArrat = Array.from(data, x => {
      x.price = Math.round(1000 * Math.random() + 1010);
      x.timestamp = Date.now();
      return x
    });
    return mynewArrat
  };

  function findEmitterById(id, array) {
    let Id = Number(id);
    let matches = array.filter(emitter => emitter.id == Id);
    let shEmitter = matches.shift();
    return shEmitter;
  };
  // function sendGraphData(id) {
  //   let Id = Number(id);
  //   let data = {};
  //   data.price = Math.round(1000 * Math.random() + 1010);
  //   data.timeMark = Date.now();
  //   data.id = Id;
  //   return data
  // };
  function sendGraphArray(id) {
    let Id = Number(id);
    let time = 60 * 5 / 2;    // 5 min / uptime (30 sec)
    let array = [];
    let i = 0;
    do {
      let data = {};
      data.price = Math.round(1000 * Math.random() + 1010);
      let now = new Date().setMinutes(i);
      data.timeMark = now;
      data.id = Id;
      array.push(data);
      console.log("data", data);
      i++
    } while (i < time);
    return array;
  };
  $httpBackend.whenGET("http://localhost:3000/home/shares")
    .respond(200, generatorSharesList(data2));
  $httpBackend.whenGET(/\/home\/shares\/(\d+)/, undefined, ["shEmitId"])
    .respond(function (method, url, data, headers, params) {
      let result = findEmitterById(params.shEmitId, data2);
      if (result == null) {
        return [404, undefined, {}];
      }
      return [200, result, {}]
    });
  $httpBackend.whenGET(/\/home\/getGraphData\/(\d+)/, undefined, ["shEmitId"])
    .respond(function (method, url, data, headers, params) {
      let result = sendGraphArray(params.shEmitId);
      if (result == null) {
        return [404, undefined, {}];
      }
      return [200, result, {}]
    });
  $httpBackend.whenGET(/*\/main\/*/).passThrough();
  $log.debug("runBlock end");
}

