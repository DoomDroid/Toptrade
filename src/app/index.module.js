import {config} from "./index.config";
import {routerConfig} from "./index.route";
import {runBlock} from "./index.run";
import {NavbarDirective} from "../app/components/navbar/navbar.directive";
import {graphDirective} from "../app/components/graph/graph.directive";
import {SharesEmittersComponent} from "./components/sharesEmitters/sharesEmitters.component";
import {EmitterComponent} from "./components/emitter/emitter.component.js";
import {MainComponent} from "./components/main/main.component.js";

angular.module("topTrader", ["ngAnimate", "ngCookies",
  "ngTouch", "ngSanitize", "ngMessages", "ngAria",
  "ui.router", "ngMaterial", "toastr", "ngMockE2E"])
  .constant("d3", window.d3)
  .config(config)
  .config(routerConfig)
  .run(runBlock)
  .directive("acmeNavbar", NavbarDirective)
  .directive("shaGraph", graphDirective)
  .component("sharesEmitter", SharesEmittersComponent)
  .component("emitterComp", EmitterComponent)
  .component("mainComp", MainComponent)
  .service("SharesEmittersService", function ($http) {

    let service = {
      getEmittersList: function () {
        return $http.get("http://localhost:3000/home/shares").then(function (response) {
          return response.data
        }).catch(function (error) {
          console.log("XHR Failed for getEmittersList.\n" + angular.toJson(error.data, true));
        })
      },
      serchEmitter: function (shEmitId) {
        return $http.get("http://localhost:3000/home/shares/" + shEmitId).then(function (response) {
          return response.data
        }).catch(function (error) {
          console.log("XHR Failed for searchEmitter.\n" + angular.toJson(error.data, true));
        })
      },
      getGraphData: function (shEmitId) {
        return $http.get("http://localhost:3000/home/getGraphData/" + shEmitId).then(function (response) {
          return response.data
        }).catch(function (error) {
          console.log("XHR Failed for searchEmitter.\n" + angular.toJson(error.data, true));
        })
      },
      getEmitter: function (id) {
        function emitterMatchesParam(emitter) {
          return emitter.id === id;
        }

        return service.getEmittersList().then(function (emittersList) {
          return emittersList.find(emitterMatchesParam)
        });
      }
    };
    return service;

  });
